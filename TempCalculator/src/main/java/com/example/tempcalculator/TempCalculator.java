package com.example.tempcalculator;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class TempCalculator extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Calculator calculator;

    public void init() throws ServletException {
        calculator = new Calculator();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Fahrenheit: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Temperature in Celsius: " +
                Double.toString(calculator.getTemperature(request.getParameter("celsius"))) +
                "</BODY></HTML>");
    }


}

class Calculator {
    public double getTemperature(String celsius){
        return Double.parseDouble(celsius)*1.8+32;

    }
}
